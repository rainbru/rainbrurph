#!/bin/sh   

sudo add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse"

sudo apt-get update -qq

# Now for Ubuntu Trusty
sudo apt-get install -y libncurses5-dev tcl8.5-dev tk8.5-dev check \
     libargtable2-dev libsdl2-dev cmake cmake-data libsdl2-net-dev \
     mongodb-dev libwebsockets-dev libpcre3-dev libegl1-mesa-dev \
     libgles2-mesa-dev libcurl4-gnutls-dev lcov curl

