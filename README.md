# rainbrurph

A complete rewrite of RainbruRPG in ansi C.

# Dependencies

On a Debian GNU/Linux, you'll have to run :

	sudo apt install libsdl2-dev libsdl2-net-dev libwebsockets-dev \
		libargtable2-dev tcl8.6-dev tk8.6-dev check

On arch-based linuxes :

	sudo pacman -S sdl2 sdl2_net libwebsockets lcov argtable

# Development

This project is built using *cmake* and uses git submodules. To correctly 
update submodules and build them :

	git submodule init
	git submodule update
	mkdir build
	cd build
	cmake ..
	make
	make check
