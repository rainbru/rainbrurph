Here is an exaplanations of subdirectories (mainly libraries) :

R?? | Stand for           | Comment
:---|:--------------------|:-------------------------------------
rml | Modularized Launcher| Multi module priority-based launcher
rtl | Type Library        | For low level types (lists/strings). 
rga | Graphic abstraction | 3D engine
