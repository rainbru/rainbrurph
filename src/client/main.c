/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \file client/main.c
  * The client's main file
  *
  *
  */

#include <stdio.h>
#include <assert.h>

#include <argtable2.h>

#include <SDL_net.h>

#include "rainbrurph_config.h"
#include "rtl/logger.h"
#include "rtl/modules.h"
#include "rtl/list.h"

#include "rga/renderer_list.h"
#include "rga/renderer_list.h"

#include "rml/launcher_list.h"

#include "wsmeta.h"

/** The client's main entry 
  *
  *
  */
int 
main (int argc, char *argv[])
{
  const char* progname = "rainbrurph-client";
  struct arg_file *dir = arg_file0("m","modules","<output>",
			 "modules loading directory (default is \"./\")");
  struct arg_file *log = arg_file0("l","logfile","<filename>",
	 "the client log file name (default is \"rainbrurph-client.log\")");

  struct arg_lit  *help  = arg_lit0("?","help","print this help and exit");
  struct arg_lit  *vers  = arg_lit0("v","version",
				    "print version information and exit");
  struct arg_end  *end   = arg_end(20);
  void* argtable[] = {dir, log, help, vers, end};
  int exitcode=0;
  int nerrors;

  /* verify the argtable[] entries were allocated sucessfully */
  if (arg_nullcheck(argtable) != 0)
    {
      /* NULL entries were detected, some allocations must have failed */
      printf("%s: insufficient memory\n",progname);
      exitcode=1;
      goto exit;
    }

  /* set any command line default values prior to parsing */
  dir->filename[0] = "./";
  log->filename[0] = "rainbrurph-client.log";
  
  /* Parse the command line as defined by argtable[] */
  nerrors = arg_parse(argc,argv,argtable);

    /* special case: '--help' takes precedence over error reporting */
    if (help->count > 0)
      {
        printf("Usage: %s", progname);
        arg_print_syntax(stdout,argtable,"\n");
        printf("Remove (unlink) the specified file(s).\n\n");
        arg_print_glossary(stdout,argtable,"  %-20s %s\n");
        printf("\nReport bugs to <no-one> as this is just an example program.\n");
        exitcode=0;
        goto exit;
      }
    /* special case: '--version' takes precedence error reporting */
    if (vers->count > 0)
      {
	printf("%s\n", APPNAME);
	exitcode=0;
        goto exit;
      }

    /* If the parser returned any errors then display them and exit */
    if (nerrors > 0)
      {
        /* Display the error details contained in the arg_end struct.*/
        arg_print_errors(stdout,end,progname);
        printf("Try '%s --help' for more information.\n",progname);
        exitcode=1;
        goto exit;
      }

  logger_create(true, true, log->filename[0], LL_DEBUG);
  log_intro(APPNAME "-client", APPVERS);

  list_t* renderer_list = renderer_list_create();
  modules_load(renderer_list, "./modules/rga*.so");
  // If we have only one renderer, we're sure it's only the dummy one
  if (list_length(renderer_list) < 2)
    {
      LOGE("No renderer modules found, can't run");
      goto exit;
    }

  // Run the best launcher found (in fact the first one, but since
  // the module list is prefixed with a number, first must be the best)
  list_t* launcher_list = launcher_list_create();
  modules_load(launcher_list, "./modules/rml*.so");

  // here is the fallback used to choose the first fully implemented launcher
  launcher_list_item_t* launch;
  list_t* first_l = list_find(launcher_list, &launcher_list_item_check);
  if (first_l == NULL)
    {
      LOGE("Can't find a suitable launcher. Exiting...");
      exit(1);
    }
  launch = (launcher_list_item_t*)first_l->value;

  // Prints selected launcher name and execute it
  char namelog[140];
  sprintf(namelog, "Calling %s exec function",  launch->name);
  LOGI(namelog);
  launch->init(APPNAME, APPVERS);
  assert(launch->exec && "Launcher's exec function not implemented or not set");
  int chosen = launch->exec(renderer_list);
  
  // Get the chosen renderer and initialize it
  renderer_list_item_t* current_renderer = (renderer_list_item_t*)
    list_get_by_index(renderer_list, chosen)->value;
  supported_options_t* so = current_renderer->supported();
  renderer_options_t* selected = renderer_options_create();
  launch->options(so, selected);
  renderer_options_dump(selected);

  current_renderer->init(APPNAME, APPVERS, selected);

  // Free the launcher list
  launcher_list_free(&launcher_list);

  // libwsmeta first test : will certainly be used in a gamestate
  LOGI("Launching meta... searching for server");
  wsmeta_t* meta = wsmeta_create();
  if (wsmeta_entry(meta, "http://www.rainbrurpg.org/rainbrurph.list", 1000))
    {
      // Entry list successfully downloaded
      
    }
  else
    {
      LOGE("Cannot read meta-server entry list. Exiting...");
      exit(2);
    }
  // Uses 'meta'
  wsmeta_free(&meta);
  
  // Network test, will be remove
  IPaddress ip;		/* Server address */
  TCPsocket sd;		/* Socket descriptor */
  int quit, len;
  char buffer[512];
  
  /* Simple parameter checking */
  /*    if (argc < 3)
    {
      fprintf(stderr, "Usage: %s host port\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  */
  if (SDLNet_Init() < 0)
    {
      fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
 
  /* Resolve the host we are connecting to */
  if (SDLNet_ResolveHost(&ip, argv[1], atoi(argv[2])) < 0)
    {
      fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
 
  /* Open a connection with the IP provided (listen on the host's port) */
  if (!(sd = SDLNet_TCP_Open(&ip)))
    {
      fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
 
  /* Send messages */
  quit = 0;
  /*  while (!quit)
    {
      printf("Write something:\n>");
      scanf("%s", buffer);
 
      len = strlen(buffer) + 1;
      if (SDLNet_TCP_Send(sd, (void *)buffer, len) < len)
	{
	  fprintf(stderr, "SDLNet_TCP_Send: %s\n", SDLNet_GetError());
	  exit(EXIT_FAILURE);
	}
 
      if(strcmp(buffer, "exit") == 0)
	quit = 1;
      if(strcmp(buffer, "quit") == 0)
	quit = 1;
    }
  */
  SDLNet_TCP_Close(sd);
  SDLNet_Quit();

  /* Delete our opengl context, destroy our window, and shutdown SDL */
  // Else, try to launch OpenGL renderer
  current_renderer->free();

 exit:
    LOGI("Cleaning rga module list");
    renderer_list_free(renderer_list);
    //unregister_all_modules();
    logger_free();

    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
    
    return exitcode;
};
