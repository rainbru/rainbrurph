/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rtl/logger.h"
#include "rtl/list.h"

#include "rga/renderer_list.h"
#include "rga/supported_options.h"

#include <stdio.h>
#include <string.h>

/* This is a dummy renderer (i.e. it will draw nothing)
 *
 */

static renderer_list_item_t* rli;
static void** dl_handle;
static supported_options_t* supported = NULL;

void module_init(const char* appname, const char* version,
		 renderer_options_t* supported)
{

}

void module_free(void)
{

}

void 
module_unregister(void)
{
  /*
  if (supported)
   supported_options_free(&supported);
   
  LOGD("Unregistreing Dummy rga module must free renderer_list_item");
  renderer_list_item_free(&rli);
  LOGI("Unregistering Dummy rga module");
  module_unload(dl_handle);
  */
}

supported_options_t*
module_supported(void)
{
  supported = supported_options_create();
  supported->resolution = false;
  supported->fullscreen = false;
  supported->double_buffer = false;
  supported->gl_depth = false;
  return supported;
}

/* The function called by rtl/modules
 *
 * Must register the module.
 * Must be calleble via dlopen()
 */
void register_module(void* dlhandle)
{
  dl_handle = &dlhandle;

  rli = renderer_list_item_create("Dummy");
  rli->init      = &module_init;
  rli->free      = &module_free;
  rli->unreg     = &module_unregister;
  rli->supported = &module_supported;

  register_renderer_module(rli);
}
