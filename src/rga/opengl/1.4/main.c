/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rtl/logger.h"
#include "rtl/list.h"
#include "rtl/modules.h"

#include "rga/renderer_list.h"
#include "rga/sdl_common.h"
#include "rga/supported_options.h"

#include <stdio.h>
#include <string.h>
#include <stdio.h>   // for the sprintf() function

#include <sys/types.h>
#include <sys/syscall.h>

#include <GL/gl.h>

static renderer_list_item_t* rli;
static void** dl_handle;

static SDL_Window*   window;
static SDL_GLContext context;

static supported_options_t* supported = NULL;

void module_init(const char* appname, const char* version,
		 renderer_options_t* supported)
{
  LOGI("Initializing OpenGL 1.4 renderer");

  char tmp[120];
  sprintf(tmp, "%s v%s", appname, version);
  window  = create_window(tmp, supported);
  context = create_glcontext(window);

  test_gl(window);

}

void module_free(void)
{
  LOGI("Freeing OpenGL 1.4 renderer");
  delete_glcontext(context);
  delete_window(window);
}

void unregister_module(void)
{
  if (supported)
    supported_options_free(&supported);

  LOGD("Unregistreing OpenGL 1.4 rga module must free renderer_list_item");
  renderer_list_item_free(&rli);
  LOGI("Unregistering OpenGL 1.4 rga module");

  module_unload(dl_handle);
}

supported_options_t*
module_supported(void)
{
  supported = supported_options_create();
  supported->resolution = true;
  get_video_modes(supported->resolutions);
  supported->fullscreen = true;
  supported->double_buffer = true;
  supported->gl_depth = true;
  get_depth_modes(supported->gl_depths);
  return supported;
}


/* The function called by rtl/modules
 *
 * Must register the module.
 * Must be calleble via dlopen()
 */
void register_module(void* dlhandle)
{
  // Test OpenGL version
#ifdef GL_VERSION_1_3
  LOGI("OpenGL 1.4 detected, registering module");
  dl_handle = &dlhandle;
  rli = renderer_list_item_create("OpenGL 1.4");
  rli->init      = &module_init;
  rli->free      = &module_free;
  rli->unreg     = &unregister_module;
  rli->supported = &module_supported;
  register_renderer_module(rli);
#else
  LOGE("Cannot register OpenGL 1.4 renderer module");
#endif
}
