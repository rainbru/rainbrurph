/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "renderer_list.h"

// Uses syscall() : hard-to-fix implicit declaration warning
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include "rtl/logger.h"

#include <stddef.h>  // for NULL pointer
#include <stdio.h>   // for the sprintf() function
#include <stdlib.h>  // For malloc()
#include <string.h>  // For strdup()


#include <sys/types.h>
#include <glob.h>

#include <dlfcn.h>

static list_t* renderers;

renderer_list_item_t*
renderer_list_item_create(const char* name)
{
  renderer_list_item_t* ret = malloc(sizeof(renderer_list_item_t));
  ret->name = strdup(name);
  ret->init      = NULL;
  ret->free      = NULL;
  ret->unreg     = NULL;
  ret->supported = NULL;
  return ret;
}


void renderer_list_item_free(renderer_list_item_t** rli)
{
  free((*rli)->name);
  free(*rli);
}

list_t* renderer_list_create()
{
  renderers = list_create();
  return renderers;
}

/* Note: this function is knwon to cause a segfault if you call it with a 
 * list_t** argument.
 */
void renderer_list_free(list_t* renderer_list)
{
  LOGI("Calling unregister for all registered modules");
  
  if (!renderer_list)
    {
      LOGE("renderer_list is NULL!");
    }
  else
    {
      char str[80];
      sprintf(str, "Unregistering '%zu' renderers",
	      list_length(renderer_list));
      LOGI(str);
    }

  // Unregister all registered modules
  LIST_FOREACH(renderer_list, rli, renderer_list_item_t*)
    {
      if (!rli->name)
	LOGE("Renderer name is NULL");
      
      char tmp[140];
      sprintf(tmp, "Calling unregister for module '%s'", rli->name);
      LOGD(tmp);

      if (rli->unreg)
	{
	  char tmp2[60];
	  sprintf(tmp2, "Calling rli->unreg() on '%u'\n", rli->unreg);
	  LOGD(tmp2);
	  // FIXME: the following line is actually causing a segfault.
	  // not the  content of the called function, but the call itself.
	  //	  rli->unreg();
	}
      else
	LOGW("unreg pointer is NULL, this may cause segfault");
    }
  LIST_FOREACH_END;

  // Free the list
  LOGI("Now freeing the list");
  if (renderer_list)
    list_free(&renderer_list);

}

void register_renderer_module(renderer_list_item_t* rli)
{

  pid_t tid = (pid_t) syscall (SYS_gettid);
  char tstr[80];
  sprintf(tstr, "In thread '%u' ...", tid);
  LOGI(tstr);

  
  char str[80];
  sprintf(str, "Starting '%s' renderer registration...", rli->name);
  LOGI(str);
  
  list_append(renderers, rli);

  char str2[80];
  sprintf(str2, "The renderer list now contains '%zu' elements",
	  list_length(renderers));
  LOGD(str2);

}
