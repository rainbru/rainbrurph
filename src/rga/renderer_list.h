/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _RENDERER_LIST_H_
#define _RENDERER_LIST_H_

#include "rtl/list.h"

#include "supported_options.h"
#include "renderer_options.h"

typedef void (*rga_module_init_t)(const char* appname, const char* version,
				  renderer_options_t* supported);
typedef void (*rga_module_free_t)(void);
typedef void (*rga_module_unregister_t)(void);
typedef supported_options_t* (*rga_module_supported_t)(void);

typedef struct
{
  char*                   name;
  rga_module_init_t       init;      // The initialization function pointer
  rga_module_free_t       free;      // The finalization function pointer
  rga_module_unregister_t unreg;     // Unregister the needed module
  rga_module_supported_t  supported; // Get supported options
}renderer_list_item_t;

renderer_list_item_t* renderer_list_item_create(const char*);
void renderer_list_item_free(renderer_list_item_t** );

list_t* renderer_list_create();
void renderer_list_free(list_t*);

void register_renderer_module(renderer_list_item_t*);

#endif // _RENDERER_LIST_H_
