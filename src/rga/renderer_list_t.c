/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "renderer_list_t.h"

#include "rtl/logger.h"
#include "renderer_list.h"

renderer_list_item_t* test_render_list_one_item;

void unregister_dummy()
{
  LOGW("Unregistering");
  // Nothing to do here
  renderer_list_item_free(&test_render_list_one_item );

}

// Simply test if we can create/free a string
START_TEST (test_constructor)
{
  renderer_list_item_t*i = renderer_list_item_create("aze");
  ck_assert(strcmp(i->name, "aze") == 0);
  ck_assert(i->init      == NULL);
  ck_assert(i->free      == NULL);
  ck_assert(i->unreg     == NULL);
  ck_assert(i->supported == NULL);
  renderer_list_item_free(&i );
}
END_TEST

START_TEST (test_render_list_empty)
{
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* lt = renderer_list_create();
  renderer_list_free(lt);
  logger_free();
}
END_TEST

START_TEST (test_render_list_one)
{
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* lt = renderer_list_create();
  test_render_list_one_item = renderer_list_item_create("aze");
  test_render_list_one_item->unreg = &unregister_dummy;
  list_append(lt, test_render_list_one_item);
  LOGW("Freeing the list from unit tests");
  renderer_list_free(lt);
  logger_free();
}
END_TEST

TCase* renderer_list_tc()
{
  TCase* c = tcase_create("renderer_list");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_render_list_empty);
  tcase_add_test(c, test_render_list_one);

  return c;
}
