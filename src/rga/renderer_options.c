/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "renderer_options.h"

#include <stdlib.h> // For malloc()/free()
#include <stdio.h>

renderer_options_t* 
renderer_options_create(void)
{
  renderer_options_t* this = malloc(sizeof(renderer_options_t));
  return this;
}

void
renderer_options_free(renderer_options_t** ro)
{
  free(*ro);
}

/** A simple print function for debugging purpose
  *
  */
void
renderer_options_dump(renderer_options_t* ro)
{
  printf("Fullscreen : ");
  if (ro->fullscreen)
    printf("On\n");
  else
    printf("Off\n");

  printf("Resolution : %dx%d\n", ro->screen_width, ro->screen_height );

  printf("Double buffer : ");
  if (ro->double_buffer)
    printf("On\n");
  else
    printf("Off\n");

  printf("Double buffer size : %d bits\n", ro->double_buffer_size );
    
}
