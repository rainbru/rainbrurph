/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Handles chosen options passed to the renderer.
 *
 */

#ifndef _RENDERER_OPTIONS_H_
#define _RENDERER_OPTIONS_H_

#include <stdbool.h>

typedef struct 
{
  bool fullscreen;
  int screen_width;
  int screen_height;
  bool double_buffer;     //!< Will SDL use the double buffer
  int double_buffer_size; //!< The double buffer size
}renderer_options_t;

renderer_options_t* renderer_options_create(void);
void                renderer_options_free(renderer_options_t**);
void                renderer_options_dump(renderer_options_t*);

#endif // _RENDERER_OPTIONS_H_
