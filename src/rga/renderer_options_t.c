/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "renderer_options_t.h"

#include "renderer_options.h"

START_TEST (test_constructor)
{
  renderer_options_t* ro = renderer_options_create();
  ck_assert(ro != NULL);
  renderer_options_free(&ro );
}
END_TEST


TCase*
renderer_options_tc()
{
  TCase* c = tcase_create("renderer_list");
  tcase_add_test(c, test_constructor);

  return c;
}
