/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sdl_common.h"

#include <SDL.h>
#include <pcre.h>

#include "rtl/string.h"
#include "rtl/logger.h"

/* A simple function that prints a message, the error code returned by SDL,
 * and quits the application */
void 
sdldie(const char *msg)
{
    printf("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    exit(1);
}

void checkSDLError(int line)
{
#ifndef NDEBUG
	const char *error = SDL_GetError();
	if (*error != '\0')
	{
		printf("SDL Error: %s\n", error);
		if (line != -1)
			printf(" + line: %i\n", line);
		SDL_ClearError();
	}
#endif
}


SDL_Window* 
create_window(const char* appname, renderer_options_t* supported)
{
  SDL_Window *mainwindow; /* Our window handle */
 
    if (SDL_Init(SDL_INIT_VIDEO) < 0) /* Initialize SDL's Video subsystem */
        sdldie("Unable to initialize SDL"); /* Or die on error */

    // Turn on double buffering according to the entered answer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, (int)supported->double_buffer);

    /* Set the Z buffer.*/
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, supported->double_buffer_size);

    /* Handle fullscreen flag */
    SDL_WindowFlags flags;
    if (supported->fullscreen)
      {
	flags = SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN;
      }
    else
      {
	flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
      }
    
    /* Create the window with selected renderer options */
    mainwindow = SDL_CreateWindow(appname, SDL_WINDOWPOS_CENTERED, 
				  SDL_WINDOWPOS_CENTERED,
        supported->screen_width, supported->screen_height, flags);
    if (!mainwindow) /* Die if creation failed */
        sdldie("Unable to create window");

    checkSDLError(__LINE__);

    return mainwindow;
}


SDL_GLContext 
create_glcontext(SDL_Window* window)
{
  SDL_GLContext maincontext;

  /* Create our opengl context and attach it to our window */
  maincontext = SDL_GL_CreateContext(window);
  checkSDLError(__LINE__);

  return maincontext;
}


void
test_gl(SDL_Window* window)
{
 
    /* This makes our buffer swap syncronized with the monitor's vertical refresh */
    SDL_GL_SetSwapInterval(1);
 
    /* Clear our buffer with a red background */
    glClearColor ( 1.0, 0.0, 0.0, 1.0 );
    glClear ( GL_COLOR_BUFFER_BIT );
    /* Swap our back buffer to the front */
    SDL_GL_SwapWindow(window);
    /* Wait 2 seconds */
    SDL_Delay(2000);
 
    /* Same as above, but green */
    glClearColor ( 0.0, 1.0, 0.0, 1.0 );
    glClear ( GL_COLOR_BUFFER_BIT );
    SDL_GL_SwapWindow(window);
    SDL_Delay(2000);
 
    /* Same as above, but blue */
    glClearColor ( 0.0, 0.0, 1.0, 1.0 );
    glClear ( GL_COLOR_BUFFER_BIT );
    SDL_GL_SwapWindow(window);
    SDL_Delay(2000);

}

void
delete_glcontext(SDL_GLContext context)
{
  SDL_GL_DeleteContext(context);
}

void
delete_window(SDL_Window* window)
{
  SDL_DestroyWindow(window);
  SDL_Quit();
}

void
get_video_modes(list_t* l)
{

  SDL_Rect** modes;
  SDL_DisplayMode displaymode = { SDL_PIXELFORMAT_UNKNOWN, 0, 0, 0, 0 };
  
  SDL_Init(SDL_INIT_VIDEO);
  int num = SDL_GetNumVideoDisplays();
  /* For instance, I only get 1280x1024, I need more resolutions
   * To test for parsing
   */
  if (num > 1)
    {
      for(int i = 0; i < num; i++){
	displaymode.format = 0;
	displaymode.w = 0;
	displaymode.h = 0;
	displaymode.refresh_rate = 0;
	displaymode.driverdata = 0;
	if(SDL_GetDisplayMode(0, i, &displaymode) < 0){
	  printf("%s\nExiting...\n", SDL_GetError());
	  SDL_Quit();
	}
	char tmp[80];
	sprintf(tmp, "%dx%d", displaymode.w, displaymode.h);
	string_t* aze = string_create(tmp);
	list_append(l, aze);
      } 
    }
  else
    {
     // Since we can't get SDL resolutions, we had some predefined ones
     SDL_Quit();
     string_t* r1 = string_create("640x480");
     list_append(l, r1);
     string_t* r2 = string_create("800x600");
     list_append(l, r2);
     string_t* r3 = string_create("1024x768");
     list_append(l, r3);
     string_t* r4 = string_create("1280x960");
     list_append(l, r4);
   }
}

void 
get_depth_modes(list_t* l)
{
  string_t* r1 = string_create("16-bit");
  list_append(l, r1);
  string_t* r2 = string_create("24-bit");
  list_append(l, r2);
  string_t* r3 = string_create("32-bit");
  list_append(l, r3);

}

/** Get a resolution string and return it to width/height
  *
  *
  * \param res the resolution string aka 1024x768
  *
  * \return true on success, false if the parsing failed
  *
  */
bool
resolution_to_wh(char* res, int* w, int* h)
{
  pcre* reCompiled;
  pcre_extra *pcreExtra;
  
  char *aStrRegex;
  const char *pcreErrorStr;
  int pcreErrorOffset;
  int subStrVec[30];

  const char *psubStrMatchStr;
  int pcreExecRet;
  
  aStrRegex = "(^[0-9]+)x([0-9]+$)";

  // First, the regex string must be compiled.
  reCompiled=pcre_compile(aStrRegex, 0, &pcreErrorStr, &pcreErrorOffset, NULL);

  if(reCompiled == NULL) {
    printf("ERROR: Could not compile '%s': %s\n", aStrRegex, pcreErrorStr);
    return false;
  }

  // Optimize the regex
  pcreExtra = pcre_study(reCompiled, 0, &pcreErrorStr);
  if(pcreErrorStr != NULL) {
    printf("ERROR: Could not study '%s': %s\n", aStrRegex, pcreErrorStr);
    return false;
  }

  /* Try to find the regex in res, and report results. */
  pcreExecRet = pcre_exec(reCompiled, pcreExtra, res, // Last is the string
			  strlen(res),  // length of string
			  0,                      // Start looking at this point
			  0,                      // OPTIONS
			  subStrVec,
			  30);        

  if(pcreExecRet < 0) { // Something bad happened..
    return false;
  }
  else {   /* We have a match ! */
        
      // At this point, rc contains the number of substring matches found...
      if(pcreExecRet == 0) {
        printf("But too many substrings were found to fit in subStrVec!\n");
        // Set rc to the max number of substring matches possible.
        pcreExecRet = 30 / 3;
      } 
      for(int j=0; j<pcreExecRet; j++) {
        pcre_get_substring(res, subStrVec, pcreExecRet, j, &(psubStrMatchStr));
      } /* end for */

      /* Get two substrings */
      const char* wStr;
      pcre_get_substring(res, subStrVec, pcreExecRet, 1, &(wStr));
      *w = atoi(wStr);
      pcre_get_substring(res, subStrVec, pcreExecRet, 2, &(wStr));
      *h = atoi(wStr);
      
      // Free up the substring
      pcre_free_substring(psubStrMatchStr);
    }
  return true;
}

/** 
 * The string must be of the form (depth)* where depth is an int
 */
bool depth_to_int(char* str, int* depth)
{
  pcre* reCompiled;
  pcre_extra *pcreExtra;
  
  char *aStrRegex;
  const char *pcreErrorStr;
  int pcreErrorOffset;
  int subStrVec[30];

  const char *psubStrMatchStr;
  int pcreExecRet;
  
  aStrRegex = "(^[0-9]+).*";

  // First, the regex string must be compiled.
  reCompiled=pcre_compile(aStrRegex, 0, &pcreErrorStr, &pcreErrorOffset, NULL);

  if(reCompiled == NULL) {
    printf("ERROR: Could not compile '%s': %s\n", aStrRegex, pcreErrorStr);
    return false;
  }

  // Optimize the regex
  pcreExtra = pcre_study(reCompiled, 0, &pcreErrorStr);
  if(pcreErrorStr != NULL) {
    printf("ERROR: Could not study '%s': %s\n", aStrRegex, pcreErrorStr);
    return false;
  }

  /* Try to find the regex in str, and report results. */
  pcreExecRet = pcre_exec(reCompiled, pcreExtra, str, // Last is the string
			  strlen(str),  // length of string
			  0,                      // Start looking at this point
			  0,                      // OPTIONS
			  subStrVec,
			  30);        

  if(pcreExecRet < 0) { // Something bad happened..
    return false;
  }
  else {   /* We have a match ! */
        
      // At this point, rc contains the number of substring matches found...
      if(pcreExecRet == 0) {
        printf("But too many substrings were found to fit in subStrVec!\n");
        // Set rc to the max number of substring matches possible.
        pcreExecRet = 30 / 3;
      } 
      for(int j=0; j<pcreExecRet; j++) {
        pcre_get_substring(str, subStrVec, pcreExecRet, j, &(psubStrMatchStr));
      } /* end for */

      /* Get the substring */
      const char* wStr;
      pcre_get_substring(str, subStrVec, pcreExecRet, 1, &(wStr));
      *depth = atoi(wStr);
      
      // Free up the substring
      pcre_free_substring(psubStrMatchStr);
    }
  return true;
}
