/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Defines some function used by all SDL-powered rga modules
 *
 */

#ifndef _SDL_COMMON_H_
#define _SDL_COMMON_H_

#include <stdbool.h>  // Uses bool type

#include "rtl/list.h"

#include "rga/renderer_options.h"

#include <GL/gl.h>
#include <SDL.h>


SDL_Window*   create_window(const char*, renderer_options_t*);
void          delete_window(SDL_Window*);

SDL_GLContext create_glcontext(SDL_Window*);
void          delete_glcontext(SDL_GLContext);

void test_gl(SDL_Window* window);
void get_video_modes(list_t*);
void get_depth_modes(list_t*);

// Option parsing functions
bool resolution_to_wh(char*, int* w, int* h);
bool depth_to_int(char*, int* depth);

#endif // _SDL_COMMON_H_
