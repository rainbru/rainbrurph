/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sdl_common_t.h"

#include "sdl_common.h"

START_TEST (sdl_common_resolution_to_wh1)
{
  int w, h;
  bool b = resolution_to_wh("768x640", &w, &h);
  ck_assert(b); // function should return true
  ck_assert(w == 768);
  ck_assert(h == 640);
}
END_TEST

START_TEST (sdl_common_resolution_to_wh2)
{
  // Should fail because of extra characters
  int w, h;
  bool b;

  b = resolution_to_wh("768x640am", &w, &h);
  ck_assert(!b);
  b = resolution_to_wh("wbc768x640", &w, &h);
  ck_assert(!b);
  b = resolution_to_wh("768xuu640", &w, &h);
  ck_assert(!b);
}
END_TEST

START_TEST (sdl_common_resolution_to_wh3)
{
  // Should fail because of missing x
  int w, h;
  bool b;

  b = resolution_to_wh("768X640", &w, &h);
  ck_assert(!b);
}
END_TEST

START_TEST (sdl_common_resolution_to_wh4)
{
  // Should fail because of a missing substring
  int w, h;
  bool b;

  b = resolution_to_wh("768X", &w, &h);
  ck_assert(!b);
}
END_TEST

START_TEST (sdl_common_depth_to_int_1)
{
  // Should success because starts with an int
  int depth;
  bool b;

  b = depth_to_int("16-Bits", &depth);
  ck_assert(b && depth == 16);
}
END_TEST

START_TEST (sdl_common_depth_to_int_2)
{
  // Should fail because it doesn't start with an int
  int depth;
  bool b;

  b = depth_to_int("-Bits", &depth);
  ck_assert(!b);
}
END_TEST

TCase* sdl_common_tc()
{
  TCase* c = tcase_create("renderer_list");
  tcase_add_test(c, sdl_common_resolution_to_wh1);
  tcase_add_test(c, sdl_common_resolution_to_wh2);
  tcase_add_test(c, sdl_common_resolution_to_wh3);
  tcase_add_test(c, sdl_common_resolution_to_wh4);
  
  tcase_add_test(c, sdl_common_depth_to_int_1);
  tcase_add_test(c, sdl_common_depth_to_int_2);

  return c;
}
