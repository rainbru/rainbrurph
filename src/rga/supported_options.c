/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \file supported_options.c
  *  Handles supported options
  *
  */

#include "supported_options.h"

#include <stdlib.h> // For free(), malloc()

/** Create a new supported_options_t pointer with default values
  *
  * Default boolean values are all set to \c false. 
  * supported_options_t::resolutions and supported_options_t::gl_depths
  * are valid pointer to empty lists.
  *
  * \return A new supported_options_t pointer
  *
  */
supported_options_t* 
supported_options_create()
{
  supported_options_t* this = malloc(sizeof(supported_options_t));
  this->resolution    = false;
  this->fullscreen    = false;
  this->double_buffer = false;
  this->gl_depth      = false;

  this->resolutions = list_create();
  this->gl_depths   = list_create();
  return this;
}

/** Free the memory used by given supported_options_t structure
  *
  * It will also free memory associated with pointer members (
  * supported_options_t::resolutions and supported_options_t::gl_depths).
  *
  */
void
supported_options_free(supported_options_t** so)
{
  list_free(&(*so)->resolutions);
  (*so)->resolutions = NULL;
  list_free(&(*so)->gl_depths);
  (*so)->gl_depths = NULL;

  free(*so);
  *so = NULL;
}

/** Simply count supported options in the provided supported_options_t
  * structure
  *
  * \param o The supported options structure
  *
  * \return The number of user modifiable options.
  *
  */
int 
supported_options_count(supported_options_t* o)
{
  int c = 0;
  if (o->resolution)    ++c;
  if (o->fullscreen)    ++c;
  if (o->double_buffer) ++c;
  if (o->gl_depth)      ++c;
  return c;
}
