/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**\file supported_options.h
  * Handles supported options
  *
  */

#ifndef _SUPPORTED_OPTIONS_H_
#define _SUPPORTED_OPTIONS_H_

#include <stdbool.h> // For booleans

#include "rtl/list.h"

/** This is the list of options supported by a given renderer
  *
  * Apointer to a new supported_options_t can be created with 
  * supported_options_create().
  *
  */
typedef struct
{
  bool    resolution;  //!< Is multiple resolutions supported ?
  list_t* resolutions; //!< Supported resolutions only as char*

  bool fullscreen;     //!< Is fullscreen supported ?
  bool double_buffer;  //!< Is double buffer supported ?

  bool    gl_depth;    //!< Is depth supported
  list_t* gl_depths;   //!< Supported depth buffer sizes as a list of int*
}supported_options_t;


supported_options_t* supported_options_create();
void                 supported_options_free(supported_options_t**);

int supported_options_count(supported_options_t*);
#endif // _SUPPORTED_OPTIONS_H_
