/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "supported_options_t.h"

#include "supported_options.h"

#include <stdbool.h> // for booleans
#include "rtl/list.h"

START_TEST (test_constructor)
{
  supported_options_t* so = supported_options_create();
  ck_assert(so != NULL);
  supported_options_free(&so);
}
END_TEST

// Test if all boolean values set to false
START_TEST (test_constructor_false)
{
  supported_options_t* so = supported_options_create();
  ck_assert(so->resolution == false);
  ck_assert(so->fullscreen == false);
  ck_assert(so->double_buffer == false);
  ck_assert(so->gl_depth == false);
  supported_options_free(&so);
}
END_TEST

// Tests if constructor correctly create inside lists
START_TEST (test_constructor_lists_not_null)
{
  supported_options_t* so = supported_options_create();
  ck_assert(so->resolutions != NULL);
  ck_assert(so->gl_depths != NULL);
  supported_options_free(&so);
}
END_TEST

// If lists are unintialized, it should cause a segfault
START_TEST (test_constructor_lists_append)
{
  supported_options_t* so = supported_options_create();
  ck_assert(list_append(so->resolutions, NULL) != NULL);
  ck_assert(list_append(so->gl_depths, NULL) != NULL);
  supported_options_free(&so);
}
END_TEST


TCase* supported_options_tc()
{
  TCase* c = tcase_create("supported_options");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_constructor_false);
  tcase_add_test(c, test_constructor_lists_not_null);
  tcase_add_test(c, test_constructor_lists_append);
  return c;
}
