/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "tests.h"

#include <check.h>

#include "renderer_list_t.h"
#include "supported_options_t.h"
#include "renderer_options_t.h"
#include "sdl_common_t.h"

Suite *
rga_suite(void)
{
  Suite* s = suite_create("rga");

  suite_add_tcase(s, renderer_list_tc());
  suite_add_tcase(s, supported_options_tc());
  suite_add_tcase(s, renderer_options_tc());
  suite_add_tcase(s, sdl_common_tc());

  return s;
}
