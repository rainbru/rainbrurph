/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rtl/logger.h"
#include "rtl/list.h"
#include "rtl/modules.h"

#include "rml/launcher_list.h"
#include "rtl/list.h"
#include "rtl/string.h"
#include "rga/renderer_list.h"
#include "rga/supported_options.h"
#include "rga/renderer_options.h"
#include "rga/sdl_common.h"

#include <stdio.h>
#include <string.h>
#include <stdio.h>   // for the sprintf() function
#include <stdbool.h>

#include "options.h"


static launcher_list_item_t* lli;
static void** dl_handle;

int module_exec(list_t* renderer_list)
{
  int idx = 0;
  printf("\nPlease choose the best renderer:\n");
  LIST_FOREACH(renderer_list, item, renderer_list_item_t*)
    {
      printf("  %d) %s\n", ++idx, item->name);
    }
  LIST_FOREACH_END;

  int max = list_length(renderer_list);

  int choice;
  int retieved = scanf("%d", &choice);
  if (retieved == 1 && retieved != EOF)
    {
      if (choice < 0 || choice > max )
	return module_exec(renderer_list) -1;
      else
	return choice -1;
    }
  else
    {
      return module_exec(renderer_list);
    }
}

void module_init(const char* appname, const char* version)
{
  char c;
  printf("Welcome to the %s-client v%s CLI launcher :\n", appname, version);
  printf("Press ENTER to continue\n");
  int retieved = scanf("%c", &c);
  if (retieved == 1 && retieved != EOF)
    {
      LOGE("Cannot get user input");
    }
}

void module_free(void)
{
  LOGI("Freeing OpenGL 1.3 renderer");
}

void unregister_module(void)
{
  LOGD("Unregistreing OpenGL 1.3 rga module must free renderer_list_item");

  module_unload(dl_handle);
}

void
module_options(supported_options_t* supported, renderer_options_t* selected)
{
  int c = supported_options_count(supported);
  if (c == 0)
    {
      printf("Sorry, the selected renderer doesn't support any option.\n\n");
      return;
    }
  
  printf("This renderer supports %d options. Please answer these questions\n\n",
	 c);
  if (supported->resolution)
    {
      int r = options_from_list("Please select a resolution", 
				supported->resolutions);
      list_t* li = list_get_by_index(supported->resolutions, r);
      string_t* str = li->value;

      resolution_to_wh(str->native, &selected->screen_width,
		       &selected->screen_height);
    }

  if (supported->fullscreen)
    {
      bool b1 = options_bool("Fullscreen mode");
      selected->fullscreen = b1;
    }

  if (supported->double_buffer)
    {
      bool b2 = options_bool("Double buffering mode");
      selected->double_buffer = b2;
    }

  if (supported->gl_depth)
    {
      int r = options_from_list("Please select a depth buffer size", 
				supported->gl_depths);
      list_t* li = list_get_by_index(supported->gl_depths, r);
      string_t* str = li->value;
      int depth;
      depth_to_int(str->native, &depth);
      printf("Selected GL depth = %d\n", depth);
      selected->double_buffer_size = depth;
    }
}

/* The function called by rtl/modules
 *
 * Must register the module.
 * Must be calleble via dlopen()
 */
void register_module(void* dlhandle)
{
  dl_handle = &dlhandle;

  lli = launcher_list_item_create("CLI launcher");
  lli->init    = &module_init;
  lli->free    = &module_free;
  lli->unreg   = &unregister_module;
  lli->exec    = &module_exec;
  lli->options = &module_options;

  LOGI("Registering 'CLI launcher' module");
  register_launcher_module(lli);
}
