/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "options.h"

#include <stdio.h>

#include "rtl/string.h"

int
options_from_list(const char* label, list_t* list)
{
  int idx = 0;
  int choice;

  printf("%s :\n\n", label);
  LIST_FOREACH(list, res, string_t*)
    {
      printf("  %d) %s\n", ++idx, res->native);
    }
  LIST_FOREACH_END;

  int max = list_length(list);


  int retieved = scanf("%d", &choice);
  if (retieved == 1 && retieved != EOF)
    {
      if (choice < 0 || choice > max)
	return options_from_list(label, list);
      else
	return choice -1;
    }
  else
    {
      return options_from_list(label, list);
    }
}

bool
options_bool(const char* label)
{
  int c;
  printf("%s :\n\n  1) Yes\n  2) No\n", label);

  int retieved = scanf("%d", &c);
  if (retieved == 1 && retieved != EOF)
    {
      if (c == 1)
	return true;
      else if (c == 2)
	return false;
    }
  return options_bool(label);
}
