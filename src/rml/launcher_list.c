/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "launcher_list.h"

// This is used to avoid an implicit declaration warning with strdup
#define _DEFAULT_SOURCE
#include <string.h>  // For strdup()

#include "rtl/logger.h"
#include "rtl/list.h"

#include <stddef.h>  // for NULL pointer
#include <stdlib.h>  // For malloc()

static list_t* launchers = NULL;

launcher_list_item_t* 
launcher_list_item_create(const char* name)
{
  launcher_list_item_t* ret = malloc(sizeof(launcher_list_item_t));
  ret->name = strdup(name);
  ret->init = NULL;
  ret->free = NULL;
  ret->unreg = NULL;
  ret->options = NULL;
  ret->exec = NULL; // Unless could cause a segfault

  return ret;
}

void
launcher_list_item_free(launcher_list_item_t** item)
{
  free((*item)->name);
  free(*item);
}


list_t*
launcher_list_create(void)
{

  launchers = list_create();
  return launchers;
}

void
launcher_list_free(list_t** l)
{
  list_free(l);
}

void
register_launcher_module(launcher_list_item_t* lli)
{
  list_append(launchers, lli);
  char str2[80];
  sprintf(str2, "The launcher list now contains '%zu' elements",
	  list_length(launchers));
  LOGD(str2);
}

/** Check if all function pointer were correctly set (i.e. not to NULL)
  *
  * This is basic check and future implementation could check for more. For
  * example, it doesn't check the name.
  *
  * The argument is of nide_t* type to be compatible with the list_find() 
  * function.
  *
  */
bool
launcher_list_item_check(struct node_t* n)
{
  launcher_list_item_t* i = (launcher_list_item_t*)n->value;
  if (i->init && i->free && i->unreg && i->exec && i->options)
    return true;

  return false;
}
