/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LAUNCHER_LIST_H_
#define _LAUNCHER_LIST_H_

#include <stdbool.h>

#include "rtl/list.h"
#include "rga/supported_options.h"
#include "rga/renderer_options.h"

typedef void (*rml_module_init_t)(const char* appname, const char* version);
typedef void (*rml_module_free_t)(void);
typedef void (*rml_module_unregister_t)(void);
/* This function, if used to let the user select the best renderer from a list,
 * must return the choosen one's index. If no choice is given to the user, 
 * return 1 (the first non-dummy renderer).
 */
typedef int (*rml_module_exec_t)(list_t*);
typedef void (*rml_module_options_t)(supported_options_t*, renderer_options_t*);

/** A launcher as represented in the list_t of launcher_list_item_t nodes
  *
  * It basically contains a name and a bunch of function pointer.
  *
  * Note: if you add a a function pointer here, please add it to the 
  * launcher_list_item_check() function test.
  *
  */
typedef struct
{
  char*                   name;
  rml_module_init_t       init;  // The initialization function pointer
  rml_module_free_t       free;  // The finalization function pointer
  rml_module_unregister_t unreg; // Unregister the needed module
  rml_module_exec_t       exec; 
  rml_module_options_t    options; 
}launcher_list_item_t;

launcher_list_item_t* launcher_list_item_create(const char*);
void                  launcher_list_item_free(launcher_list_item_t**);

list_t* launcher_list_create(void);
void    launcher_list_free(list_t**);

/* Remember : as for the renderer list, please register a module
 *   only if you think you can correctly handle the launcher thing.
 */
void register_launcher_module(launcher_list_item_t*);

bool launcher_list_item_check(struct node_t*);

#endif // _LAUNCHER_LIST_H_
