/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "launcher_list_t.h"

#include "rtl/logger.h"
#include "rtl/list.h" // Uses list_t
#include "launcher_list.h"

// Dummy pointer to check for launcher item
void dummy_init(const char* appname, const char* version){}
void dummy_free(void){}
void dummy_unregister(void){}
int dummy_exec(list_t* l){}
void dummy_options(supported_options_t* a, renderer_options_t* b){}

// Simply test if we can create/free a string
START_TEST (test_constructor)
{
  launcher_list_item_t*i = launcher_list_item_create("aze");
  ck_assert(strcmp(i->name, "aze") == 0);
  ck_assert(i->init    == NULL);
  ck_assert(i->free    == NULL);
  ck_assert(i->unreg   == NULL);
  ck_assert(i->exec    == NULL);
  ck_assert(i->options == NULL);
  launcher_list_item_free(&i );
}
END_TEST

START_TEST (test_check_null)
{
  launcher_list_item_t*i = launcher_list_item_create("aze");
  list_t* l = list_create();
  l->value = (launcher_list_item_t*)i;

  bool b = launcher_list_item_check(i);
  // There is only NULL pointers here
  ck_assert(b == false);
  launcher_list_item_free(&i );
  list_free(&l);
}
END_TEST

START_TEST (test_check_ok)
{
  launcher_list_item_t*i = launcher_list_item_create("aze");
  // There should be no NULL pointer here
  i->init = &dummy_init;
  i->free = &dummy_free;
  i->unreg = &dummy_unregister;
  i->exec = &dummy_exec;
  i->options = &dummy_options;

  list_t* l = list_create();
  l->value = (launcher_list_item_t*)i;
  
  bool b = launcher_list_item_check((struct node_t*)l);
  ck_assert(b == true);
  launcher_list_item_free(&i );
  list_free(&l);
}
END_TEST

TCase* launcher_list_tc()
{
  TCase* c = tcase_create("launcher_list");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_check_null);
  tcase_add_test(c, test_check_ok);

  return c;
}
