/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rtl/logger.h"
#include "rtl/list.h"
#include "rtl/modules.h"

#include "rml/launcher_list.h"

#include <stdio.h>
#include <string.h>
#include <stdio.h>   // for the sprintf() function

static launcher_list_item_t* lli;
static void** dl_handle;

void module_init(const char* appname, const char* version)
{

}

void module_free(void)
{
  LOGI("Freeing OpenGL 1.3 renderer");
}

void unregister_module(void)
{
  LOGD("Unregistreing OpenGL 1.3 rga module must free renderer_list_item");

  module_unload(dl_handle);
}


/* The function called by rtl/modules
 *
 * Must register the module.
 * Must be callable via dlopen()
 */
void register_module(void* dlhandle)
{
  dl_handle = &dlhandle;

  lli = launcher_list_item_create("Tcl/Tk launcher");
  lli->init  = &module_init;
  lli->free  = &module_free;
  lli->unreg = &unregister_module;

  //  LOGI("Registering 'Tcl/Tk launcher' module");
  // NOTE: currently disable since I work on the CLI launcher
  register_launcher_module(lli);
}
