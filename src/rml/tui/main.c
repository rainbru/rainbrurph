/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* A TUI (Text User Interface) launcher using ncurses
 *
 */

#include "rtl/logger.h"
#include "rtl/list.h"
#include "rtl/modules.h"

#include "rml/launcher_list.h"

#include <stdio.h>
#include <string.h>
#include <stdio.h>   // for the sprintf() function
#include <stdlib.h>  // for the calloc() function

#include "renderer_menu.h"
#include "menu.h"

static launcher_list_item_t* lli;
static void** dl_handle;

static list_t* st_renderer_list;

int module_exec(list_t* renderer_list)
{
  st_renderer_list = renderer_list;
  init_ncurses();
  return renderer_menu(renderer_list);
}

void module_init(const char* appname, const char* version)
{

}

void module_free(void)
{
  LOGI("Freeing OpenGL 1.3 renderer");
}

void unregister_module(void)
{
  LOGD("Unregistreing OpenGL 1.3 rga module must free renderer_list_item");

  module_unload(dl_handle);
}

void
module_options(supported_options_t* supported, renderer_options_t* selected)
{
  int ret = menu(supported, selected);
  switch (ret)
    {
    case -1:
      renderer_menu(st_renderer_list);
      break;
    case -2:
      endwin();
      LOGD("Exit");
      exit(0);
      return;
      break;
    }
}


/* The function called by rtl/modules
 *
 * Must register the module.
 * Must be callable via dlopen()
 */
void register_module(void* dlhandle)
{
  dl_handle = &dlhandle;

  lli = launcher_list_item_create("TUI launcher");
  lli->init  = &module_init;
  lli->free  = &module_free;
  lli->unreg = &unregister_module;
  lli->exec  = &module_exec;
  lli->options = &module_options;

  LOGI("Registering 'TUI launcher' module");
  register_launcher_module(lli);
}
