/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h> // Uses strlen
#include <stdio.h> // Uses sprintf()

#include "menu.h"

#include "rtl/logger.h"
#include "rtl/string.h"


/** The char used to print double buffer size */
char depth[10];
char pres[10]; // The in-menu printed resolution


char *choices[] = {
  "Choice 1",
  "Choice 2",
  "Choice 3",
  "Choice 4",
  "Exit",
};


void
print_in_middle(WINDOW *win, int starty, int startx, int width,
		char *string, chtype color)
{
  int length, x, y;
  float temp;
  
  if(win == NULL)
    win = stdscr;
  getyx(win, y, x);

  if(starty != 0)
    y = starty;
  
  if(width == 0)
    width = 80;
  
  length = strlen(string);
  temp = (width - length)/ 2;
  x = startx + (int)temp;
  wattron(win, color);
  mvwprintw(win, y, x, "%s", string);
  wattroff(win, color);
  refresh();
}

void
init_ncurses()
{
  LOGI("Initializing curses");
  /* Initialize curses */
  initscr();
  start_color();
  cbreak();
  noecho();
  keypad(stdscr, TRUE);
  init_pair(1, COLOR_RED, COLOR_BLACK);
}


/** Drawth graphic options menu
  *
  * \param supp     The supported options
  * \param selected The selected options
  *
  * \return The menu result : -1 if Back, -2 if Exit 
  *
  */
int
menu(supported_options_t* supp, renderer_options_t* selected)
{
  ITEM **my_items;
  int c;				
  MENU *my_menu;
  WINDOW *my_menu_win;
  int i;


  int n_choices = supported_options_count(supp);
  
  /* Create items */
  my_items = (ITEM **)calloc(supported_options_count(supp), sizeof(ITEM *));
  if (my_items == NULL)
    {
      LOGE("Error allocating menu items memory");
      return 0; // Should fix 'Dereference after null check' coverity defect
    }
  feed_menu(my_items, supp, selected);
  
  /* Create menu */
  my_menu = new_menu((ITEM **)my_items);

  /* Create the window to be associated with the menu */
  my_menu_win = create_centered_win(40, 10, 4);

  if (my_menu_win == NULL)
    {
      LOGE("Error allocating ncurses window memory");
      return 0;
    }
  keypad(my_menu_win, TRUE);
     
  /* Set main window and sub window */
  set_menu_win(my_menu, my_menu_win);
  set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));

  /* Set menu mark to the string " * " */
  set_menu_mark(my_menu, " * ");

  /* Print a border around the main window and print a title */
  box(my_menu_win, 0, 0);
  print_in_middle(my_menu_win, 1, 0, 40, "Graphic options", COLOR_PAIR(1));
  mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
  mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
  mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
  mvprintw(LINES - 2, 0, "F7 Back - F8 Exit");
  refresh();
  
        
  /* Post the menu */
  post_menu(my_menu);
  //	wrefresh(my_menu_win); // Causes a segfault

  while((c = wgetch(my_menu_win)))
    {
      switch(c)
	{
	case  KEY_F(7):
	  return -1;
	case  KEY_F(8):
	  LOGD("Exit menu with F8");
	  return -2;
	case KEY_DOWN:
	  menu_driver(my_menu, REQ_DOWN_ITEM);
	  break;
	case KEY_UP:
	  menu_driver(my_menu, REQ_UP_ITEM);
	  break;
	case 10: // Changing the menu value
	  sub_menu(item_index(current_item(my_menu)));
	  break;
	}
      wrefresh(my_menu_win);
    }	

  
  /* Unpost and free all the memory taken up */
  unpost_menu(my_menu);
  free_menu(my_menu);
  for(i = 0; i < n_choices; ++i)
    free_item(my_items[i]);
  endwin();
}

WINDOW*
create_centered_win(int width, int height, int begin_y)
{
  int begin_x = (COLS /2) - (width / 2);
  return newwin(height, width, begin_y, begin_x);
}

void
feed_menu(ITEM** items, supported_options_t* supported,
	  renderer_options_t* selected)
{
  int count = supported_options_count(supported);
  items[count] = (ITEM *)NULL;

  int current = 0;

  if (supported->resolution)
    {
      sprintf(pres, "%dx%d", selected->screen_width, selected->screen_height);
      items[current++] = new_item("Resolution", pres);
				  
    }

  if (supported->fullscreen)
    {
      items[current++] = new_item("Fullscreen",
				  description_from_bool(selected->fullscreen));
    }

  if (supported->double_buffer)
    {
      items[current++] = new_item("Double buffer",
			       description_from_bool(selected->double_buffer));
    }

  if (supported->gl_depth)
    {
      sprintf(depth, "%d bits", selected->double_buffer_size);
      items[current++] = new_item("Double buffer size", depth);


      /* 
      // This is how to get each supported depths
      list_t* li = list_get_by_index(supported->gl_depths, 0);
      string_t* str = li->value;
      printf("Selected GL depth = %s\n", str->native);
      */
      /*
      int r = options_from_list("Please select a depth buffer size", 
				supported->gl_depths);
      list_t* li = list_get_by_index(supported->gl_depths, r);
      string_t* str = li->value;
      int depth;
      depth_to_int(str->native, &depth);
      printf("Selected GL depth = %d\n", depth);
      selected->double_buffer_size = depth;
      */
    }

}

char*
description_from_bool(bool b)
{
  if (b)
    return "On";
  else
    return "Off";
      
}


/** Opens a sub menu used to select the new value of an option
  *
  * \param section
  *
  */
void
sub_menu(int section)
{
  printf("Loading menu %d\n",section);
  WINDOW* win = create_centered_win(30, 20, 1);

  keypad(win, TRUE);
     
  /* Set main window and sub window */
  //  set_menu_win(my_menu, my_menu_win);
  //  set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));

  /* Set menu mark to the string " * " */
  //  set_menu_mark(my_menu, " * ");

  /* Print a border around the main window and print a title */
  box(win, 0, 0);
  print_in_middle(win, 1, 0, 30, "??? options", COLOR_PAIR(1));
  mvwaddch(win, 2, 0, ACS_LTEE);
  mvwhline(win, 2, 1, ACS_HLINE, 28);
  mvwaddch(win, 2, 29, ACS_RTEE);
  wrefresh(win);
  refresh();

  
}
