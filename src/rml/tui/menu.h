/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _TUI_MENU_H_
#define _TUI_MENU_H_

#include <ncurses.h>
#include <menu.h>

#include <stdlib.h> // Uses calloc()

#include "rga/supported_options.h"
#include "rga/renderer_options.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define CTRLD 	4

void print_in_middle(WINDOW*, int, int, int, char*, chtype);
int menu(supported_options_t*, renderer_options_t*);
WINDOW* create_centered_win(int, int, int);
void feed_menu(ITEM**, supported_options_t*, renderer_options_t*);

void init_ncurses();

// Menu descriptions
char* description_from_bool(bool);

// Sub menu function
void sub_menu(int);


#endif // !_TUI_MENU_H_
