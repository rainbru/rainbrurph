/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "renderer_menu.h"

#include "rga/renderer_list.h" // Uses renderer_list_item_t
#include "rtl/logger.h"        // Uses LOG* macros
#include "menu.h"              // Uses create_centered_win

#include <ncurses.h>
#include <menu.h>
#include <stdlib.h>  // Uses calloc()

int
renderer_menu(list_t* renderer_list)
{
  ITEM **my_items; // The menu items
  MENU *my_menu;
  WINDOW *my_menu_win;
  int c;
  
  // Allocate items from renderer list
  int idx = 0;
  int n_choices = list_length(renderer_list);
  my_items = (ITEM **)calloc(n_choices, sizeof(ITEM *));
  LIST_FOREACH(renderer_list, item, renderer_list_item_t*)
    {
      my_items[idx] = new_item(item->name, "");
      ++idx;
    }
  LIST_FOREACH_END;

    /* Create menu and its window */
  my_menu = new_menu((ITEM **)my_items);
  my_menu_win = create_centered_win(40, 10, 4);

  if (my_menu_win == NULL)
    {
      LOGE("Error allocating ncurses window memory");
      return -1;
    }
  keypad(my_menu_win, TRUE);

    /* Set main window and sub window */
  set_menu_win(my_menu, my_menu_win);
  set_menu_sub(my_menu, derwin(my_menu_win, 6, 38, 3, 1));

  /* Set menu mark to the string " * " */
  set_menu_mark(my_menu, " * ");

  /* Print a border around the main window and print a title */
  box(my_menu_win, 0, 0);
  print_in_middle(my_menu_win, 1, 0, 40, "Graphic renderer", COLOR_PAIR(1));
  mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
  mvwhline(my_menu_win, 2, 1, ACS_HLINE, 38);
  mvwaddch(my_menu_win, 2, 39, ACS_RTEE);
  mvprintw(LINES - 2, 0, "F8 to exit        - ENTER to select");
  refresh();
        
  /* Post the menu */
  post_menu(my_menu);

  bool stay = true;
  int return_value = 1; // Default renderer
  while(stay && (c = wgetch(my_menu_win)))
    {
      /*      if (c == KEY_F(8) || c == KEY_ENTER)
	stay = false;
	break;
      */
      switch(c)
	{
	case KEY_F(8):
	  LOGD("F8 pressed, exitting");
	  stay = false;
	  return_value = -1;
	  break;
	case 10:
	  LOGI("Enter pressed");
	  stay = false;
	  break;
	case KEY_DOWN:
	  menu_driver(my_menu, REQ_DOWN_ITEM);
	  break;
	case KEY_UP:
	  menu_driver(my_menu, REQ_UP_ITEM);
	  break;
	}
      wrefresh(my_menu_win);
    }	

  /* Unpost and free all the memory taken up */
  unpost_menu(my_menu);
  free_menu(my_menu);
  for(int i = 0; i < n_choices; ++i)
    {
      printf("Freeing menu item %d\n", i);
      free_item(my_items[i]);
    }
  endwin();
  return return_value;
}
