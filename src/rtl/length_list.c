/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "length_list.h"

#include <stddef.h>  // for NULL pointer
#include <stdlib.h>  // For malloc()

length_list_t*
length_list_create(void)
{
  length_list_t* this = malloc(sizeof(*this));
  if (this)
    {
      this->length = 0;
      this->root = malloc(sizeof(length_node_t));
      if (!this->root)
	{
	  free(this);
	  return NULL;
	}
      else
	{
	  this->root->next = NULL;
	  return this;
	}
    }
  else
    return NULL;
}

void
length_list_free(length_list_t** l)
{
  length_node_t* next;
  if ((*l)->root)
    {
      length_node_t* n = (*l)->root;
      while (n->next != NULL)
	{
	  next = n->next;
	  free(n);
	  n = next;
	}
      n = NULL;
    }
  free((*l));
  *l=NULL;
}

length_node_t*
length_list_last_node(length_list_t* l)
{
  length_node_t* n = l->root;
  while (n->next != NULL)
    n = n->next;

  return n;
}

// Returnsd the fresh new node containing the value
length_node_t*
length_list_append(length_list_t* l, void* value)
{
  // Special case, the list is empty, value is set to root's node
  if (l->length == 0)
    {
      l->root->value = value;
      ++l->length;
      return l->root;
    }
  else
    {
      length_node_t* new_node = malloc(sizeof(length_node_t));
      new_node->next = NULL;
      new_node->value = value;
      length_node_t* last = length_list_last_node(l);
      last->next = new_node;
      ++l->length;
      return new_node;
    }
  
}

length_node_t* 
length_list_get_by_index(length_list_t* list, int index)
{
  if (index < 0 || index >= list->length)
    return NULL;

  int i = 0;
  LENGTH_LIST_FOREACH(list,item,length_node_t*)
    {
      // Warning: the returned object rely on the foreach macro implementation
      if (i == index)
	return improbable_item_name; 

      ++i;
    }
  LENGTH_LIST_FOREACH_END;

  return NULL;
}
