/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* This is a specialized list with a direct access to the length
 * value
 */

#ifndef _LENGTH_LIST_H_
#define _LENGTH_LIST_H_

/* Start the iteration through a list
 *
 * Preconditions:
 * - the LIST parameter must be valid (created using list_create())
 * - the ELEM variable should *not* be declared.
 *
 * Postconditions:
 * - close the structure with list_foreach_end.
 *
 * Parameters:
 * +LIST: pointer to a valid list_t
 * +ELEM: the name of the element in the list. You don't to have to declare
 *        it before. It will have the node_t->value for each item
 * +CAST: the type of the element. Can be a pointer.
 *
 */
#define LENGTH_LIST_FOREACH(LIST,ELEM,CAST)				\
  if (LIST->length > 0) {						\
  CAST ELEM;								\
  length_node_t* improbable_item_name = LIST->root;				\
  do {									\
  ELEM = (CAST)improbable_item_name->value;

/* Closes the list_foreach iteration structure
 *
 */
#define LENGTH_LIST_FOREACH_END				\
  improbable_item_name = improbable_item_name->next; }	\
  while (improbable_item_name != NULL  );  }

typedef struct node
{
  void* value;
  struct node* next;
}length_node_t;

typedef struct list
{
  length_node_t* root;
  unsigned int length;
}length_list_t;

length_list_t* length_list_create(void);
void           length_list_free(length_list_t**);

length_node_t* length_list_last_node(length_list_t*);
length_node_t* length_list_append(length_list_t*, void*);

length_node_t* length_list_get_by_index(length_list_t* list, int index);


#endif // _LENGTH_LIST_H_
