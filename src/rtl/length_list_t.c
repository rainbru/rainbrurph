/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "length_list_t.h"

#include "length_list.h"

#include <stdlib.h>  // For malloc()

typedef struct
{
  int    i;
  double d;
}a_t;

typedef struct
{
  char* c;
}b_t;

// Simply test if we can create/free a string
START_TEST (test_constructor)
{
  length_list_t* s = length_list_create();
  ck_assert(s != NULL);
  length_list_free(&s);
  ck_assert(s == NULL);
}
END_TEST

// Root node shouldn't be NULL
START_TEST (test_constructor_node)
{
  length_list_t* s = length_list_create();
  ck_assert(s->root->next == NULL);
  length_list_free(&s);
}
END_TEST

START_TEST (test_size_0)
{
  length_list_t* s = length_list_create();
  ck_assert_int_eq(s->length, 0);
  length_list_free(&s);
}
END_TEST

START_TEST (test_last_node)
{
  length_list_t* s = length_list_create();
   length_node_t* n = length_list_last_node(s);
  ck_assert(n != NULL);
  ck_assert(n->next == NULL);
  length_list_free(&s);
}
END_TEST

START_TEST (test_last_node_2)
{
  int val = 8;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&val);
   length_node_t* n = length_list_last_node(s);
  ck_assert(n != NULL);
  ck_assert(n->next == NULL);
  length_list_free(&s);
}
END_TEST

START_TEST (test_append_length)
{
  int val = 5;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&val);
  ck_assert_int_eq(s->length, 1);
  length_list_free(&s);
}
END_TEST

// Because adding to an empty list is just a special case
START_TEST (test_append_length_2)
{
  int val = 5, val2 = 7;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&val);
  length_list_append(s, (void*)&val2);
  ck_assert_int_eq(s->length, 2);
  length_list_free(&s);
}
END_TEST


// Test if the root value isn't empty after an append call
START_TEST (test_append_to_empty)
{
  int val = 5;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&val);
  ck_assert(s->root->value != NULL);
  int vv = *(int*)s->root->value;
  ck_assert_int_eq(val, vv);
  length_list_free(&s);
}
END_TEST

START_TEST (test_append_real_values)
{
  int v1 = 5, v2 = 12, v3 = 785;

  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&v1);
  length_list_append(s, (void*)&v2);
  length_list_append(s, (void*)&v3);
  int vv1 = *(int*)s->root->value;
  int vv2 = *(int*)s->root->next->value;
  int vv3 = *(int*)s->root->next->next->value;
  ck_assert_int_eq(v1, vv1);
  ck_assert_int_eq(v2, vv2);
  ck_assert_int_eq(v3, vv3);
  
  length_list_free(&s);
}
END_TEST

START_TEST (test_append_return)
{
  int v1 = 5;
  length_list_t* s = length_list_create();
   length_node_t* app = length_list_append(s, (void*)&v1);
  ck_assert(app != NULL);
  ck_assert_int_eq(v1, *(int*)app->value);
  length_list_free(&s);
}
END_TEST

START_TEST (test_append_return_2)
{
  int v1 = 5, v2 = 11;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&v1);
   length_node_t* app = length_list_append(s, (void*)&v2);
  ck_assert(app != NULL);
  ck_assert_int_eq(v2, *(int*)app->value);
  length_list_free(&s);
}
END_TEST

START_TEST (test_foreach_manual)
{
  int v1 = 5, v2 = 11, v3 = 4;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&v1);
  length_list_append(s, (void*)&v2);
  length_list_append(s, (void*)&v3);

  int idx = 0, sum = 0;
   length_node_t* n = s->root;
  while (n != NULL)
    {
      int* i = (int*)n->value;
      ++idx; // Here we use the 
      sum += *i;
      n = n->next;
    }
  length_list_free(&s);
  
  ck_assert_int_eq(idx, 3);
  ck_assert_int_eq(sum, 20);
}
END_TEST

START_TEST (test_foreach_macro)
{
  int v1 = 15, v2 = 8, v3 = 7;
  length_list_t* s = length_list_create();
  length_list_append(s, (void*)&v1);
  length_list_append(s, (void*)&v2);
  length_list_append(s, (void*)&v3);

  int idx = 0, sum = 0;
  LENGTH_LIST_FOREACH(s, item, int*)
    {
      ++idx;
      sum += *item;
    }
  LENGTH_LIST_FOREACH_END
  length_list_free(&s);
  ck_assert_int_eq(idx, 3);
  ck_assert_int_eq(sum, 30);
}
END_TEST

START_TEST (test_foreach_empty)
{
  length_list_t* s = length_list_create();

  int idx = 0, sum = 0;

  LENGTH_LIST_FOREACH(s, item, int*)
    {
      ++idx;
      sum += *item;
    }
  LENGTH_LIST_FOREACH_END

  length_list_free(&s);
  ck_assert_int_eq(idx, 0);
  ck_assert_int_eq(sum, 0);
}
END_TEST

START_TEST (test_foreach_pointer)
{
  char* v1 = "hello";
  char* v2 = "world!";

  length_list_t* s = length_list_create();
  length_list_append(s, (void*)v1);
  length_list_append(s, (void*)v2);

  int idx = 0, sum = 0;

  LENGTH_LIST_FOREACH(s, item, char*)
    {
      ++idx;
      sum += strlen(item);
    }
  LENGTH_LIST_FOREACH_END

  length_list_free(&s);
  ck_assert_int_eq(idx, 2);
  ck_assert_int_eq(sum, 11);
}
END_TEST

START_TEST (test_struct_a)
{
  length_list_t* s = length_list_create();
  a_t* a1 = malloc(sizeof(a_t));
  a1->i = 520;
  a1->d = 20.31;
  
  length_list_append(s, a1);
  
  ck_assert_int_eq(((a_t*)s->root->value)->i, 520);
  length_list_free(&s);
  free(a1);
}
END_TEST

START_TEST (test_struct_b)
{
  length_list_t* s = length_list_create();
  b_t* a2 = malloc(sizeof(b_t));
  a2->c = "AZE";
  
  length_list_append(s, a2);
  
  ck_assert(strcmp(((b_t*)s->root->value)->c, "AZE") == 0);
  length_list_free(&s);
  free(a2);
}
END_TEST

START_TEST (test_get_by_index_1)
{
  int i1 = 7, i2 = 64, i3 = 12, i4 = 764, i5 = 60;

  length_list_t* ll = length_list_create();
  length_list_append(ll, &i1);
  length_list_append(ll, &i2);
  length_list_append(ll, &i3);
  length_list_append(ll, &i4);
  length_list_append(ll, &i5);

  length_node_t*  byi1_p;
  int byi1;
  byi1_p = length_list_get_by_index(ll, 1);
  ck_assert(byi1_p != NULL);
  byi1 = *(int*)byi1_p->value;
  ck_assert_int_eq(byi1, 64);

  byi1_p = length_list_get_by_index(ll, 3);
  ck_assert(byi1_p != NULL);
  byi1 = *(int*)byi1_p->value;
  ck_assert_int_eq(byi1, i4);

  length_list_free(&ll);
}
END_TEST

START_TEST (test_get_by_index_outbound)
{
  length_list_t* ll = length_list_create();
  ck_assert(length_list_get_by_index(ll, -1) == NULL);
  ck_assert(length_list_get_by_index(ll, 14) == NULL);
  length_list_free(&ll);
}
END_TEST

TCase* length_list_tc()
{
  TCase* c = tcase_create("length_list");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_constructor_node);
  tcase_add_test(c, test_size_0);
  tcase_add_test(c, test_last_node);
  tcase_add_test(c, test_last_node_2);
  tcase_add_test(c, test_append_length);
  tcase_add_test(c, test_append_length_2);
  tcase_add_test(c, test_append_to_empty);
  tcase_add_test(c, test_append_real_values);
  tcase_add_test(c, test_append_return);
  tcase_add_test(c, test_append_return_2);
  tcase_add_test(c, test_foreach_manual);
  tcase_add_test(c, test_foreach_macro);
  tcase_add_test(c, test_foreach_empty);
  tcase_add_test(c, test_foreach_pointer);
  tcase_add_test(c, test_struct_a);
  tcase_add_test(c, test_struct_b);
  tcase_add_test(c, test_get_by_index_1);
  tcase_add_test(c, test_get_by_index_outbound);
  return c;
}
