/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "list.h"

#include <stdlib.h>  // For malloc()

list_t* 
list_create(void)
{
  list_t* ret = malloc(sizeof(list_t));
  ret->value  = NULL;
  ret->next = NULL;
  return ret;
}

void
list_free(list_t** l)
{
  free(*l);
  *l = NULL;
}

// Return the last node of a list (i.e. the first element with NULL next value)
list_t* 
list_last_node(list_t* list)
{
  struct node_t* n = list;
  while (n->next != NULL)
    n = n->next;

  return n;
}


list_t*
list_append(list_t* list, void* item)
{
  
  if (list->value == NULL)
    {
      list->value = item;
      return list;
    }
  else
    {
      struct node_t* n = malloc(sizeof(struct node_t));
      n->value = item;
      n->next = NULL;
      list_t* last = list_last_node(list);
      last->next = n;
      return n;
    }
}

size_t  
list_length(list_t* list)
{
  if (list->value == NULL)
    return 0;

  size_t i = 0;
  struct node_t* n = list;
  while (n != NULL)
    {
      n = n->next;
      ++i;
    }
  return i;
}

list_t* 
list_get_by_index(list_t* list, int index)
{
  int idx = 0;
  struct node_t* n = list;
  while (n != NULL)
    {
      if (idx == index)
	return n;

      ++idx;
      n = n->next;
    }
  return NULL;
}


/** Returns the first node of the list that satisfies the test callback
  *
  * If no node is applicable (all returns false), it returns NULL.
  *
  */
list_t*
list_find(list_t* list, find_callback_t condition)
{
  // Can't use LIST_FOREACH because I can't tell the value cast
  struct node_t* n = list;
  while (n != NULL)
    {
      // Here we call the callback
      if (condition(n))
	return n;

      n = n->next;
    }
  // If we didn't found any true item, return NULL
  return NULL;
}
