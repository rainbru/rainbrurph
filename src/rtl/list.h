/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LIST_H_
#define _LIST_H_

#include <stddef.h> // for size_t
#include <stdbool.h> // for bool

/* Her is the infamous foreach loop
 *
 * The old_list cast was :
 * ELEM = *(CAST*)improbable_item_name->value;
 * maybe it was what caused the segfault on the rtl/modules unit tests.
 *
 * The && special case in while loop is needed for empty lists, i.e. when
 * the list pointer isn't NULL but no value added to this first node_t.
 *
 * NOTE: if you use the list with a pointer to a scalar (int*), you will
 *       have to set the pointed value as CAST (int*) and dereference the
 *       ELEM by yourself to use it : `int sum += *element`
 *
 */
#define LIST_FOREACH(LIST,ELEM,CAST)                                    \
  CAST ELEM;                                                            \
  struct node_t* improbable_item_name = LIST;				\
  while (improbable_item_name != NULL					\
	 && improbable_item_name->value != NULL){			\
  ELEM = (CAST)improbable_item_name->value;

#define LIST_FOREACH_END                                \
  improbable_item_name = improbable_item_name->next; }

typedef struct node_t{
   void*          value;
   struct node_t* next;
}list_t;

/** The find callback signature 
  *
  * Must return true if node passes the condition.
  *
  */
typedef bool (*find_callback_t)(struct node_t*);

list_t* list_create(void);
void    list_free(list_t**);

list_t* list_append(list_t* list, void* item);
list_t* list_last_node(list_t* list);
list_t* list_get_by_index(list_t* list, int index);
size_t  list_length(list_t*); // Please use %zu in printf format string

list_t* list_find(list_t*, find_callback_t);

#endif // _LIST_H_
