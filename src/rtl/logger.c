/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logger.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

const char* BLACK   ="\033[22;30m"; //!< Espace sequence for black
const char* RED     ="\033[22;31m"; //!< Espace sequence for red
const char* GREEN   ="\033[22;32m"; //!< Espace sequence for green
const char* BROWN   ="\033[22;33m"; //!< Espace sequence for brown
const char* BLUE    ="\033[22;34m"; //!< Espace sequence for blue
const char* MAGENTA ="\033[22;35m"; //!< Espace sequence for magenta
const char* CYAN    ="\033[22;36m"; //!< Espace sequence for cyan
const char* GRAY    ="\033[22;37m"; //!< Espace sequence for gray
const char* DGRAY   ="\033[01;30m"; //!< Espace sequence for dark gray
const char* LRED    ="\033[01;31m"; //!< Espace sequence for light red
const char* LGREEN  ="\033[01;32m"; //!< Espace sequence for light green
const char* YELLOW  ="\033[01;33m"; //!< Espace sequence for yellow
const char* LBLUE   ="\033[01;34m"; //!< Espace sequence for light blue
const char* LCYAN   ="\033[01;36m"; //!< Espace sequence for ligth cyan
const char* WHITE   ="\033[01;37m"; //!< Espace sequence for white
const char* RESET   ="\033[0;m";    //!< Restet display attribute

logger_t* static_logger;

char* ll_str(loglevel_t ll)
{
  switch (ll)
    {
    case LL_DEBUG:
      return "DD";
    case LL_INFO:
      return "II";
    case LL_WARNING:
      return "WW";
    case LL_ERROR:
      return "EE";
    }
}

void log_nothing(loglevel_t l,char* filename, char* line, char* msg)
{
  // does nothing
}

void log_std(loglevel_t l,char* filename, char* line, char* msg)
{
  /*  printf("%s%s%s %s %s\n",COLOR_LOG(CYAN, filename),COLOR_LOG(BLACK, ":")
	 line, ll_str(l), msg);
  */
  printf("%s%s%s%s%s%s", CYAN, filename, RED, ":", GREEN, line);
  printf("%s %s %s%s\n", YELLOW, ll_str(l), RESET, msg);
}

void log_file(loglevel_t l,char* filename, char* line, char* msg)
{
  time_t t = time(NULL);
  struct tm* tm = localtime(&t);
  char buffer [80];
  strftime (buffer,80,"%F  %T",tm);

  fprintf(static_logger->file, "%s -- %s:%s %s %s\n",
	  buffer, filename, line, ll_str(l), msg);

  fflush(static_logger->file);
}

void logger_create(bool logstd, bool logfile, const char* filename,
		   loglevel_t min_level)
{

  
  static_logger=malloc(sizeof(logger_t));
  static_logger->tostd     = &log_nothing;
  static_logger->min_level = min_level;

  if (logstd) 
    static_logger->tostd = &log_std;
  else
    static_logger->tostd = &log_nothing;
  
  if (logfile)
    {
      static_logger->tofile = &log_file;
      static_logger->file   = fopen(filename, "w+");
    }
  else
    {
      static_logger->tofile = &log_nothing;
      static_logger->file = NULL;
    }
}

void logger_free()
{
  if (static_logger->file)
    {
      fclose(static_logger->file);
      static_logger->tofile = NULL;
    }

  free(static_logger);
  static_logger = NULL;
  
}

void log_message(loglevel_t l,char* filename, char* line, char* msg)
{
  if (l>=static_logger->min_level)
    {
      static_logger->tostd(l, filename, line, msg);
      static_logger->tofile(l, filename, line, msg);
    }
}

void log_intro_to_file(FILE* f, const char* appname, const char* version)
{
  time_t t = time(NULL);
  struct tm* tm = localtime(&t);
  char buffer [80];
  strftime (buffer,80,"%F %T",tm);

  fprintf(f, "    Starting %s v%s on %s\n\n", appname, version, buffer);
  fprintf(f, "  DD = Debug\n  II = Information\n");
  fprintf(f, "  WW = Warning\n  EE = Error\n");
  fprintf(f, "\n    Will not log messages under %s level.\n\n",
	  ll_str(static_logger->min_level));
}

void log_intro(const char* appname, const char* version)
{
  log_intro_to_file(stdout, appname, version);
  if (static_logger->file)
    log_intro_to_file(static_logger->file, appname, version);
}

