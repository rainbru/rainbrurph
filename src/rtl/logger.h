/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <stdbool.h>
#include <stdio.h>

typedef enum{
  LL_DEBUG  = 0,
  LL_INFO,
  LL_WARNING,
  LL_ERROR
}loglevel_t;

/// A macro used to fix a __LINE__ bug
#define STRINGIFY(x) #x

/// A macro used to stringify the __LINE__ value
#define TOSTRING(x) STRINGIFY(x)

#define LOG(LEVEL, MSG)						\
  log_message(LEVEL, __FILENAME__, TOSTRING(__LINE__), MSG)


#define LOGD(MSG) LOG(LL_DEBUG,   MSG)
#define LOGI(MSG) LOG(LL_INFO,    MSG)
#define LOGW(MSG) LOG(LL_WARNING, MSG)
#define LOGE(MSG) LOG(LL_ERROR,   MSG)

typedef void (*output_ptr_t)(loglevel_t l,char* filename, char* line, char*);

typedef struct
{
  output_ptr_t tostd;
  output_ptr_t tofile;
  FILE* file;
  loglevel_t min_level;
}logger_t;

void logger_create(bool logstd, bool logfile, const char* filename,
		   loglevel_t min_level);
void logger_free();

void log_message(loglevel_t l,char* filename, char* line, char*);
void log_intro(const char* appname, const char* version);

extern logger_t* static_logger;

#endif // _LOGGER_H_
