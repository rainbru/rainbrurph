/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logger_t.h"

#include "logger.h"
#include <unistd.h>

// Simply test if we can create/free a string
START_TEST (test_constructor)
{
  logger_create(true, false, "filename", LL_ERROR);
  ck_assert(static_logger->tostd != NULL);
  ck_assert(static_logger->tofile != NULL);
  ck_assert(static_logger->file == NULL);
  LOG(LL_INFO, "Sample message");
  logger_free();
}
END_TEST

START_TEST (test_constructor_file)
{
  char* filename = "rainbrurph-tests-logfile";
  logger_create(false, true, filename, LL_DEBUG);
  ck_assert(static_logger->tostd  != NULL);
  ck_assert(static_logger->tofile != NULL);
  ck_assert(static_logger->file   != NULL);
  logger_free();

  if (unlink(filename) !=0)
    printf("Cannot remove log file named '%s'\n", filename);
}
END_TEST

TCase* logger_tc()
{
  TCase* c = tcase_create("logger");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_constructor_file);

  return c;
}
