/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rtl/modules.h"

// Uses syscall() : hard-to-fix implicit declaration warning
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

#include <stddef.h>  // for NULL pointer
#include <glob.h>

#include <dlfcn.h>

#include <sys/types.h>

#include "logger.h"

void modules_load_one(list_t* renderer_list, const char* soname)
{
  register_function reg;
  char* err;

  void* handle = dlopen(soname, RTLD_NOW); //RTLD_LAZY);
  if (handle == NULL)
    {
      char tstr[140], terr[140];
      sprintf(tstr, "Can't load module '%s' ...", soname);
      LOGE(tstr);
      sprintf(terr, "  *%s*", dlerror());
      LOGE(terr);
      // Avoid a segfault when trying to register a module that can't be found
      return;  
    }

  reg = dlsym(handle, "register_module");
  if ((err = dlerror()) != NULL) 
    LOGE("Can't find the 'register_module' function");
 
  (*reg)(handle);
}
void module_unload(void** dl_handle)
{
  LOGI("Unloading module");
  if (*dl_handle)
      dlclose(*dl_handle);
  else
    LOGE("Can't close a NULL dlopen handle");
}

unsigned int modules_load(list_t* renderer_list, const char*pattern)
{
  glob_t globbuf;
  int i;

  pid_t tid = (pid_t) syscall (SYS_gettid);
  char tstr[80];
  sprintf(tstr, "Loading modules from thread '%u' ...", tid);
  LOGI(tstr);

  //globbuf.gl_offs = 1;
  if (glob(pattern, GLOB_ERR, NULL, &globbuf) != 0)
    LOGE("Can't glob so names in modules");
  
  // use it
  for (i=0; i<globbuf.gl_pathc; ++i)
    {
      modules_load_one(renderer_list, globbuf.gl_pathv[i]);
    }

  globfree(&globbuf);
  return i;
}

