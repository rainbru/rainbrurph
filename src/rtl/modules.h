/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Loads all modules for a given glob pattern
 *
 * The interresting idea, is that this function is reusable, whatever
 * the module type to be registered.
 *
 */

#ifndef _MODULES_H_
#define _MODULES_H_

#include "list.h"

// dlhandle: the dlopen returned handle
typedef void (*register_function)(void* dlhandle);

unsigned int modules_load(list_t*, const char* pattern);
void         module_unload(void** dl_handle);

void modules_load_one(list_t* renderer_list, const char* soname);


#endif // _MODULES_H_

