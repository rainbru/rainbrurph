/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "modules_t.h"

#include "rtl/logger.h"
#include "rtl/modules.h"

#include "rga/renderer_list.h"

#include "rml/launcher_list.h"

#include <stddef.h>

// These are hardcoded correct modules number
static int implemented_renderer_modules = 2;
static int implemented_launcher_modules = 3;

START_TEST (test_modules_load_unexisting)
{
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* rl = renderer_list_create();
  modules_load_one(rl, "./modules/rga-unknown-module-name.so");
  renderer_list_free(rl);
  logger_free();
}
END_TEST

START_TEST (test_modules_load_one)
{
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* rl = renderer_list_create();
  // Note: calling another rga module that dummy (i.e. opengl-1.4)
  // may cause a segfault here
  modules_load_one(rl, "./modules/rga-dummy.so");
  // Seems to cause a segfault
  renderer_list_free(rl);
  LOGW("Now freeing the list");
  logger_free();
}
END_TEST

START_TEST (test_renderer_number)
{
  unsigned int i = 0;
  // We at least need a valid logger and a renderer list to avoid segfaults
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* rl = renderer_list_create();
  // The following line is the segfault
  i = modules_load(rl, "./modules/rga*.so");
  ck_assert_int_eq(i, implemented_renderer_modules);
  renderer_list_free(rl);
  logger_free();
}
END_TEST

START_TEST (test_launcher_number)
{
  // We at least need a valid logger and a renderer list to avoid segfaults
  logger_create(true, false, "ranbrurpg-tests.log", LL_DEBUG);
  list_t* rl = launcher_list_create();
  unsigned int i = modules_load(rl, "./modules/rml*.so");
  ck_assert_int_eq(i, implemented_launcher_modules);
  launcher_list_free(&rl);
  logger_free();
}
END_TEST


TCase* modules_tc()
{
  TCase* c = tcase_create("modules");
  tcase_add_test(c, test_modules_load_unexisting);
  tcase_add_test(c, test_modules_load_one);
  tcase_add_test(c, test_renderer_number);
  tcase_add_test(c, test_launcher_number);

  return c;
}
