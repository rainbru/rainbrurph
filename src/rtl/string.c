/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "string.h"

#include <string.h> // For strlen and strncopy

//#define NULL 0
#include <stdlib.h> // For free(), malloc()

string_t* string_create(char native[])
{
    string_t* this= malloc(sizeof(string_t));
    this->length = strlen(native);
    this->native = malloc(this->length+1);
    if (this->native)
      {
	strncpy(this->native, native, this->length+1);
      }
    else
      {
	free(this->native);
	free(this);
	this=NULL;
      }
    return this;
}

void string_free(string_t** str)
{
    free((*str)->native);
    free((*str));
    *str=NULL;
}
