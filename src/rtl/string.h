/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**\file string.c
  * A string with easy length access
  *
  */

#ifndef _STRING_H_
#define _STRING_H_

#include <stddef.h> // for size_t

/** A string type with easy length access
  *
  */
typedef struct string {
  size_t length;   //!< The lenght of the string
  char *native;    //!< The underlying char pointer
} string_t;

string_t* string_create(char* native);
void string_free(string_t** str);

#endif // _STRING_H_
