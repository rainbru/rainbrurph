/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "string_t.h"

#include "string.h"
#include <string.h>

// Simply test if we can create/free a string
START_TEST (test_constructor)
{
  string_t* s = string_create("aze");
  string_free(&s);
}
END_TEST

// Test if the constructor correctly set the content string
START_TEST (test_constructor_set)
{
  string_t* s = string_create("This is a string");
  ck_assert(strcmp("This is a string", s->native) == 0);
  string_free(&s);
}
END_TEST

START_TEST (test_constructor_len)
{
  string_t* s = string_create("This is a string");
  ck_assert_int_eq(s->length, 16);
  string_free(&s);
}
END_TEST


TCase* string_tc()
{
  TCase* c = tcase_create("string");
  tcase_add_test(c, test_constructor);
  tcase_add_test(c, test_constructor_set);
  tcase_add_test(c, test_constructor_len);

  return c;
}
