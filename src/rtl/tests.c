/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "tests.h"

#include "length_list_t.h"
#include "list_t.h"
#include "logger_t.h"
#include "string_t.h"
#include "modules_t.h"

Suite * rtl_suite(void)
{
  Suite* s = suite_create("rtl");

  suite_add_tcase(s, length_list_tc());
  suite_add_tcase(s, list_tc());
  suite_add_tcase(s, logger_tc());
  suite_add_tcase(s, string_tc());
  suite_add_tcase(s, modules_tc());

  return s;
}
