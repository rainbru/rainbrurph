/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** \file server/main.c
  * The serv's main file.
  *
  */

#include "rainbrurph_config.h"
#include <stdio.h>

#include <argtable2.h>
#include <SDL_net.h>

#include "rtl/string.h"
#include "rtl/logger.h"

#include "wsmeta.h"


/** The server's main function 
  *
  */
int 
main (int argc, char *argv[])
{

  const char* progname = "rainbrurpg-server";
  struct arg_int  *pport = arg_int0("p","port",NULL,
				    "Defines the listening TCP port");
  struct arg_file *log = arg_file0("l","logfile","<filename>",
	 "the client log file name (default is \"rainbrurph-server.log\")");

  struct arg_lit  *help  = arg_lit0("?","help","print this help and exit");
  struct arg_lit  *vers  = arg_lit0("v","version",
				    "print version information and exit");
  struct arg_end  *end   = arg_end(20);
  void* argtable[] = {pport, log, help, vers, end};
  int exitcode=0;
  int nerrors;

  /* verify the argtable[] entries were allocated sucessfully */
  if (arg_nullcheck(argtable) != 0)
    {
      /* NULL entries were detected, some allocations must have failed */
      printf("%s: insufficient memory\n",progname);
      exitcode=1;
      goto exit;
    }

  /* set any command line default values prior to parsing */
  pport->ival[0] = 2000;
  log->filename[0] = "rainbrurph-server.log";
  
  /* Parse the command line as defined by argtable[] */
  nerrors = arg_parse(argc,argv,argtable);

  /* special case: '--help' takes precedence over error reporting */
  if (help->count > 0)
    {
      printf("Usage: %s", progname);
      arg_print_syntax(stdout,argtable,"\n");
      printf("Remove (unlink) the specified file(s).\n\n");
      arg_print_glossary(stdout,argtable,"  %-20s %s\n");
      printf("\nReport bugs to <no-one> as this is just an example program.\n");
      exitcode=0;
      goto exit;
    }
  /* special case: '--version' takes precedence error reporting */
  if (vers->count > 0)
    {
      printf("%s\n", APPNAME);
      exitcode=0;
      goto exit;
    }
  
  /* If the parser returned any errors then display them and exit */
  if (nerrors > 0)
    {
      /* Display the error details contained in the arg_end struct.*/
      arg_print_errors(stdout,end,progname);
      printf("Try '%s --help' for more information.\n",progname);
      exitcode=1;
      goto exit;
    }
  
  logger_create(true, true, log->filename[0], LL_DEBUG);
  log_intro(APPNAME "-server", APPVERS);
  
  LOGI("Server should be started...");

  printf("Starting %s server\n", APPNAME);

  // Working with meta server
  wsmeta_t* meta =  wsmeta_create();
  bool bmeta = wsmeta_entry(meta, "http://rainbrurpg.org", 500);

  
  /*  if (SDL_Init(SDL_INIT_TIMER) != 0) {
    fprintf(stderr,
	    "\nUnable to initialize SDL:  %s\n",
	    SDL_GetError()
	    );
    return 1;
  }
  atexit(SDL_Quit);
  */

  // from http://content.gpwiki.org/index.php/SDL:Tutorial:Using_SDL_net
  TCPsocket sd, csd; /* Socket descriptor, Client socket descriptor */
  IPaddress ip, *remoteIP;
  int quit, quit2;
  char buffer[512];
 
  if (SDLNet_Init() < 0)
    {
      fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
 
  /* Resolving the host using NULL make network interface to listen */
  Uint16 port = pport->ival[0];
  if (SDLNet_ResolveHost(&ip, NULL, port) < 0)
    {
      fprintf(stderr, "SDLNet_ResolveHost: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
  printf("Server is listening on port %d\n", port);
 
  /* Open a connection with the IP provided (listen on the host's port) */
  if (!(sd = SDLNet_TCP_Open(&ip)))
    {
      fprintf(stderr, "SDLNet_TCP_Open: %s\n", SDLNet_GetError());
      exit(EXIT_FAILURE);
    }
  
  /* Wait for a connection, send data and term */
  quit = 0;
  while (!quit)
    {
      /* This check the sd if there is a pending connection.
       * If there is one, accept that, and open a new socket for communicating */
      if ((csd = SDLNet_TCP_Accept(sd)))
	{
	  /* Now we can communicate with the client using csd socket
	   * sd will remain opened waiting other connections */
	  
	  /* Get the remote address */
	  if ((remoteIP = SDLNet_TCP_GetPeerAddress(csd)))
	    /* Print the address, converting in the host format */
	    printf("Host connected: %x %d\n", SDLNet_Read32(&remoteIP->host), SDLNet_Read16(&remoteIP->port));
	  else
	    fprintf(stderr, "SDLNet_TCP_GetPeerAddress: %s\n", SDLNet_GetError());
	  
	  quit2 = 0;
	  while (!quit2)
	    {
	      if (SDLNet_TCP_Recv(csd, buffer, 512) > 0)
		{
		  printf("Client say: %s\n", buffer);
		  
		  if(strcmp(buffer, "exit") == 0)	/* Terminate this connection */
		    {
		      quit2 = 1;
		      printf("Terminate connection\n");
		    }
		  if(strcmp(buffer, "quit") == 0)	/* Quit the program */
		    {
		      quit2 = 1;
		      quit = 1;
		      printf("Quit program\n");
		    }
		}
	    }
	  
	  /* Close the client socket */
	  SDLNet_TCP_Close(csd);
	}
    }
  
  SDLNet_TCP_Close(sd);
  SDLNet_Quit();

  wsmeta_free(&meta);
  
 exit:
  /* deallocate each non-null entry in argtable[] */
  arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
  logger_free();
  return exitcode;
};
