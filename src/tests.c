/*
 * Copyright 2015-2018 Jerome Pasquier
 *
 * This file is part of rainbrurph.
 *
 * rainbrurph is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rainbrurph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rainbrurph.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>  // For EXIT_* macros
#include <check.h>

#include "rtl/tests.h"
#include "rga/tests.h"
#include "rml/tests.h"

int main(void)
{
  int number_failed;
  Suite *s;
  SRunner *sr;

  // Adds test suites
  sr  = srunner_create(rtl_suite());
  srunner_add_suite(sr, rga_suite());
  srunner_add_suite(sr, rml_suite());

  srunner_run_all(sr,  CK_NORMAL);

  number_failed  = srunner_ntests_failed(sr);
  
  srunner_free(sr);
  
  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
